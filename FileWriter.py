
def create_file_formula(name, n_var, formula):
    f = open(name, "w")
    n_clauses = len(formula)
    f.write("p cnf " + str(n_var)+ " " + str(n_clauses) + "\n")
    for c in formula:
        for l in c:
            f.write(str(l) + " ")
        f.write("0\n")
    f.close()
