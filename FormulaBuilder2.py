from TetrominoList import TetrominoList
from VarHandler2 import VarHandler2
from FormulaBuilder import FormulaBuilder
import random

class FormulaBuilder2(FormulaBuilder):
    def __init__(self, K, I, J, T):
        assert(0 < K and 0 < I and 0 < J and 0 < T)
        FormulaBuilder.__init__(self, K, I, J)
        self.T = T
        # Ugly : we build two VarHandler but use only one
        self.v_h = VarHandler2(self.t_list, K, I, J, T)

    def at_least_one_curr_shape(self):
        ''' at least one curr_shape(t, l) must be true for each t '''
        res = []
        for t in range(self.T):
            res += [[self.v_h.curr_shape(t, l) for l in range(self.L)]]
        return res

    def at_most_one_curr_shape(self):
        ''' at most one curr_shape(t, l) must be true for each t '''
        res = []
        for t in range(self.T):
            for l1 in range(self.L - 1):
                for l2 in range(l1+1, self.L):
                    res += [[-self.v_h.curr_shape(t, l1), -self.v_h.curr_shape(t, l2)]]
        return res
    
    def at_least_one_curr_x(self):
        ''' at least one curr_x(t, i) must be true for each t '''
        res = []
        for t in range(self.T):
            res += [[self.v_h.curr_x(t, i) for i in range(self.I)]]
        return res

    def at_most_one_curr_x(self):
        ''' at most one curr_x(t, i) must be true for each t '''
        res = []
        for t in range(self.T):
            for i1 in range(self.I - 1):
                for i2 in range(i1+1, self.I):
                    res += [[-self.v_h.curr_x(t, i1), -self.v_h.curr_x(t, i2)]]
        return res

    def at_least_one_curr_y(self):
        ''' at least one curr_y(t, j) must be true for each t '''
        res = []
        for t in range(self.T):
            res += [[self.v_h.curr_y(t, j) for j in range(self.J)]]
        return res

    def at_most_one_curr_y(self):
        ''' at most one curr_y(t, j) must be true for each t '''
        res = []
        for t in range(self.T):
            for j1 in range(self.J - 1):
                for j2 in range(j1+1, self.J):
                    res += [[-self.v_h.curr_y(t, j1), -self.v_h.curr_y(t, j2)]]
        return res
        

    def semantic_curr_shapexy(self):
        ''' impose that curr_shapexy(t, l, i, j) is equivalent to
        curr_shape(t, l) and curr_x(t, i) and curr_y(t, j) '''
        impl1 = []
        impl2 = []
        for t in range(self.T):
            for l in range(self.L):
                for i in range(self.I):
                    for j in range(self.J):
                        # First implication
                        hyp1 = [-self.v_h.curr_shapexy(t, l, i, j)]
                        impl1 += [hyp1 + [self.v_h.curr_shape(t, l)]]
                        impl1 += [hyp1 + [self.v_h.curr_x(t, i)]]
                        impl1 += [hyp1 + [self.v_h.curr_y(t, j)]]
                        # Second implication
                        impl2 += [[-self.v_h.curr_shape(t, l), -self.v_h.curr_x(t, i), -self.v_h.curr_y(t, j), self.v_h.curr_shapexy(t, l, i, j)]]
        return impl1 + impl2

    def constraining_fixed(self):
        ''' constraining fixed(k, t) vraiables '''
        res = []
        # If tetromino k+1 is fixed at time t, then tetromino k must also be fix
        for t in range(self.T):
            for k in range(self.K - 1):
                res += [[-self.v_h.fixed(k+1, t), self.v_h.fixed(k, t)]]
        # No unfixing
        for k in range(self.K):
            for t in range(self.T-1):
                res += [[-self.v_h.fixed(k, t), self.v_h.fixed(k, t+1)]]
        # None fixed at the beginning, all fixed at the end
        for k in range(self.K-1):
            res += [[-self.v_h.fixed(k, 0)], [self.v_h.fixed(k, self.T-1)]]
        return res
        
    
    def not_too_near_walls_curr(self):
        ''' prevent falling tetrominos from intersecting with the right or the
        left of the board '''
        res = []
        for t in range(self.T):
            for l in range(self.L):
                min_i = self.t_list.min_x_coordinate(l)
                for i in range(-min_i):
                    res += [[-self.v_h.curr_shape(t, l), -self.v_h.curr_x(t, i)]]
                max_i = self.t_list.max_x_coordinate(l)
                for i in range(self.I - max_i, self.I):
                    res += [[-self.v_h.curr_shape(t, l), -self.v_h.curr_x(t, i)]]
        return res

    def not_intersecting(self):
        ''' prevent falling tetrominos from overlapping with already placed ones '''
        res = []
        for k in range(self.K):
            for t in range(self.T):
                for l in range(self.L):
                    min_i = self.t_list.min_x_coordinate(l)
                    max_i = self.t_list.max_x_coordinate(l)
                    min_j = self.t_list.min_y_coordinate(l)
                    max_j = self.t_list.max_y_coordinate(l)
                    for i in range(-min_i, self.I-max_i):
                        for j in range(-min_j, self.J):
                            pre = [-self.v_h.curr_shapexy(t, l, i, j), -self.v_h.fixed(k, t)]
                            for (di, dj) in self.t_list.tetromino_list[l]:
                                i2 = i+di
                                j2 = j+dj
                                if j2 < self.J:
                                    # The falling tetromino may go beyond the top
                                    res += [pre + [-self.v_h.cell(k, i2, j2)]]
        return res
    
    def possible_movement(self):
        ''' define how a falling tetromino may move '''
        res = []
        for t in range(self.T-1):
            for l in range(self.L):
                min_i = self.t_list.min_x_coordinate(l)
                max_i = self.t_list.max_x_coordinate(l)
                min_j = self.t_list.min_y_coordinate(l)
                max_j = self.t_list.max_y_coordinate(l)
                for i in range(-min_i, self.I-max_i):
                    for j in range(max(1, -min_j), self.J):
                        pre = [-self.v_h.curr_shapexy(t, l, i, j)]
                        #The tetromino be left as is
                        pre+= [self.v_h.curr_shapexy(t, l, i, j)]
                        #The tetromino may be moved to the left
                        if self.t_list.within_bounds(l, i-1, j, self.I, self.J):
                            pre+= [self.v_h.curr_shapexy(t+1, l, i-1, j)]
                        #The tetromino may be moved to the right
                        if self.t_list.within_bounds(l, i+1, j, self.I, self.J):
                            pre+= [self.v_h.curr_shapexy(t+1, l, i+1, j)]
                        #The tetromino may fall by 1
                        if self.t_list.within_bounds(l, i, j-1, self.I, self.J):
                            pre+= [self.v_h.curr_shapexy(t+1, l, i, j-1)]
                        #The tetromino may be rotated counter-clockwise
                        lp = self.t_list.get_rotation(l, 1)
                        if self.t_list.within_bounds(lp, i, j, self.I, self.J):
                            pre+= [self.v_h.curr_shapexy(t+1, lp, i, j)]
                        #The tetromino may be rotated clockwise
                        lm = self.t_list.get_rotation(l, -1)
                        if self.t_list.within_bounds(lm, i, j, self.I, self.J):
                            pre+= [self.v_h.curr_shapexy(t+1, lm, i, j)]
                        #The tetromino may get fixed
                        pre += [self.v_h.fix(t)]
                        res += [pre]
        return res

    def fix_and_fixed(self):
        res = []
        for t in range(self.T-1):
            #if not fixed(k, t) and fixed (k, t+1) then fix(t)
            for k in range(self.K):
                res += [[self.v_h.fixed(k, t), -self.v_h.fixed(k, t+1), self.v_h.fix(t)]]
            #if fix(t) and fixed(k, t) and not fixed(k+1, t) then fixed(k+1, t+1)
            for k in range(self.K-1):
                res += [[-self.v_h.fix(t), -self.v_h.fixed(k, t), self.v_h.fixed(k+1, t), self.v_h.fixed(k+1, t+1)]]
        return res

    def fixing_tetromino(self):
        ''' define the correspondance between shapexy, currshape_xy and fixed '''
        res = []
        for k in range(self.K):
            for t in range(self.T-1):
                for l in range(self.L):
                    min_i = self.t_list.min_x_coordinate(l)
                    max_i = self.t_list.max_x_coordinate(l)
                    min_j = self.t_list.min_y_coordinate(l)
                    max_j = self.t_list.max_y_coordinate(l)
                    for i in range(-min_i, self.I-max_i):
                        for j in range(max(1, -min_j), self.J):
                            res += [[-self.v_h.shapexy(k, l, i, j), -self.v_h.fixed(k, t+1), self.v_h.fixed(k, t), self.v_h.curr_shapexy(t, l, i, j)]]
        return res

    def new_tetromino(self):
        ''' make a new randomly chosen tetromino fall '''
        res = []
        for t in range(self.T-1):
            res += [[-self.v_h.fix(t), self.v_h.curr_shapexy(t+1,self.t_list.random_tetromino(), self.I//2, self.J-1)]]
        return res

    def whole_formula(self):
        ''' build the whole formula '''
        res = FormulaBuilder.whole_formula(self)
        res += self.at_least_one_curr_shape()
        res += self.at_most_one_curr_shape()
        res += self.at_least_one_curr_x()
        res += self.at_most_one_curr_x()
        res += self.at_least_one_curr_y()
        res += self.at_most_one_curr_y()
        res += self.semantic_curr_shapexy()
        res += self.constraining_fixed()
        res += self.not_too_near_walls_curr()
        res += self.not_intersecting()
        res += self.possible_movement()
        res += self.fix_and_fixed()
        res += self.fixing_tetromino()
        res += self.new_tetromino()
        return res
    
