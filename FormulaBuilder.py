from TetrominoList import TetrominoList
from VarHandler import VarHandler


class FormulaBuilder(object):
    def __init__(self, K, I, J):
        assert(0 < K and 0 < I and 0 < J)
        self.t_list = TetrominoList()
        self.L = self.t_list.L
        self.K = K
        self.I = I
        self.J = J
        self.v_h = VarHandler(self.t_list, K, I, J)

    def at_least_one_shape(self):
        ''' at least one shape(k, l) must be true for each k '''
        res = []
        for k in range(self.K):
            res += [[self.v_h.shape(k, l) for l in range(self.L)]]
        return res

    def at_most_one_shape(self):
        ''' at most one shape(k, l) must be true for each k '''
        res = []
        for k in range(self.K):
            for l1 in range(self.L - 1):
                for l2 in range(l1+1, self.L):
                    res += [[-self.v_h.shape(k, l1), -self.v_h.shape(k, l2)]]
        return res
    
    def at_least_one_x(self):
        ''' at least one x(k, i) must be true for each k '''
        res = []
        for k in range(self.K):
            res += [[self.v_h.x(k, i) for i in range(self.I)]]
        return res

    def at_most_one_x(self):
        ''' at most one x(k, i) must be true for each k '''
        res = []
        for k in range(self.K):
            for i1 in range(self.I - 1):
                for i2 in range(i1+1, self.I):
                    res += [[-self.v_h.x(k, i1), -self.v_h.x(k, i2)]]
        return res

    def at_least_one_y(self):
        ''' at least one y(k, j) must be true for each k '''
        res = []
        for k in range(self.K):
            res += [[self.v_h.y(k, j) for j in range(self.J)]]
        return res

    def at_most_one_y(self):
        ''' at most one y(k, j) must be true for each k '''
        res = []
        for k in range(self.K):
            for j1 in range(self.J - 1):
                for j2 in range(j1+1, self.J):
                    res += [[-self.v_h.y(k, j1), -self.v_h.y(k, j2)]]
        return res

    def semantic_shapexy(self):
        ''' impose that shapexy(k, l, i, j) is equivalent to
        shape(k, l) and x(k, i) and y(k, j) '''
        impl1 = []
        impl2 = []
        for k in range(self.K):
            for l in range(self.L):
                for i in range(self.I):
                    for j in range(self.J):
                        # First implication
                        hyp1 = [-self.v_h.shapexy(k, l, i, j)]
                        impl1 += [hyp1 + [self.v_h.shape(k, l)]]
                        impl1 += [hyp1 + [self.v_h.x(k, i)]]
                        impl1 += [hyp1 + [self.v_h.y(k, j)]]
                        # Second implication
                        impl2 += [[-self.v_h.shape(k, l), -self.v_h.x(k, i), -self.v_h.y(k, j), self.v_h.shapexy(k, l, i, j)]]
        return impl1 + impl2
                        
    
    def not_too_near_walls(self):
        ''' prevent tetrominos from intersecting with the right or the
        left of the board '''
        res = []
        for k in range(self.K):
            for l in range(self.L):
                min_i = self.t_list.min_x_coordinate(l)
                for i in range(-min_i):
                    res += [[-self.v_h.shape(k, l), -self.v_h.x(k, i)]]
                max_i = self.t_list.max_x_coordinate(l)
                for i in range(self.I - max_i, self.I):
                    res += [[-self.v_h.shape(k, l), -self.v_h.x(k, i)]]
        return res

    def not_too_near_top_nor_bottom(self):
        ''' prevent tetrominos from intersecting with the top or the
        bootom of the board '''
        res = []
        for k in range(self.K):
            for l in range(self.L):
                min_j = self.t_list.min_y_coordinate(l)
                for j in range(-min_j):
                    res += [[-self.v_h.shape(k, l), -self.v_h.y(k, j)]]
                max_j = self.t_list.max_y_coordinate(l)
                for j in range(self.J - max_j, self.J):
                    res += [[-self.v_h.shape(k, l), -self.v_h.y(k, j)]]
        return res

    def cell_initialisation(self):
        ''' initialize a line of cells at the bottom of the board '''
        res = []
        for i in range(self.I):
            res += [[self.v_h.cell(0, i, 0)]]
        return res
    
    def no_disappearing_cell(self):
        ''' prevents filled cell from diasapearing'''
        res = []
        for k in range(self.K):
            for i in range(self.I):
                for j in range(self.J):
                    res += [[-self.v_h.cell(k, i, j), self.v_h.cell(k+1, i, j)]]
        return res

    def no_appearing_cell(self):
        '''prevents cells from appearing when no tetromino were placed nearby '''
        res = []
        for k in range(self.K):
            for i in range(self.I):
                for j in range(self.J):
                    pre = [self.v_h.cell(k, i, j), -self.v_h.cell(k+1, i, j)]
                    for l in range(self.L):
                        T = self.t_list.tetromino_list[l]
                        for (di, dj) in T:
                            i2 = i-di
                            j2 = j-dj
                            if self.t_list.within_bounds(l, i2, j2, self.I, self.J):
                                pre += [self.v_h.shapexy(k, l, i-di, j-dj)]
                    res += [pre]
        return res
        
    
    def tetromino_placing(self):
        ''' updates the cells according to shape, x and y '''
        empty_space = []
        fill_cells = []
        on_top_of_a_cell = []
        for k in range(self.K):
            for l in range(self.L):
                min_i = self.t_list.min_x_coordinate(l)
                max_i = self.t_list.max_x_coordinate(l)
                min_j = self.t_list.min_y_coordinate(l)
                max_j = self.t_list.max_y_coordinate(l)
                for i in range(-min_i, self.I-max_i):
                    for j in range(-min_j, self.J-max_j):
                        pre = [-self.v_h.shapexy(k, l, i, j)]
                        for (di, dj) in self.t_list.tetromino_list[l]:
                            empty_space += [pre + [-self.v_h.cell(k, i+di, j+dj)]]
                            fill_cells += [pre + [self.v_h.cell(k+1, i+di, j+dj)]]
                        if j>0:
                            clause_on_top = pre
                            for di, dj in self.t_list.get_support_pos(l):
                                i2 = i+di
                                j2 = j+dj
                                if self.t_list.within_bounds(l, i2, j2, self.I, self.J):
                                    clause_on_top += [self.v_h.cell(k, i2, j2)]
                        
                                on_top_of_a_cell+= [clause_on_top]
        return empty_space + fill_cells + on_top_of_a_cell
                        
    

    def max_var_used(self):
        ''' return the number of variables used '''
        return self.v_h.max_var()
                                    
    def whole_formula(self):
        ''' build the whole formula '''
        res = []
        res += self.at_least_one_shape()
        res += self.at_most_one_shape()
        res += self.at_least_one_x()
        res += self.at_most_one_x()
        res += self.at_least_one_y()
        res += self.at_most_one_y()
        res += self.semantic_shapexy()
        res += self.not_too_near_walls()
        res += self.not_too_near_top_nor_bottom()
        res += self.cell_initialisation()
        res += self.no_disappearing_cell()
        res += self.no_appearing_cell()
        res += self.tetromino_placing()
        return res
