from VarHandler2 import VarHandler2


def check_int(s):
    if s == "":
        return False
    else:
        if s[0] in ('-', '+'):
            return s[1:].isdigit()
        return s.isdigit()

def parse(v_h, s):
    val_s = s.split('\n')[1]
    filtered_s = [a for a in val_s.split(' ') if check_int(a)]
    val = [int(a) for a in filtered_s]
    pos_var = [v_h.inverse(a) for a in val if a > 0]
    shapexy_list = [t for (s, t) in pos_var if s == "shapexy"]
    shape_list = [t for (s, t) in pos_var if s == "shape"]
    x_list = [t for (s, t) in pos_var if s == "x"]
    y_list = [t for (s, t) in pos_var if s == "y"]
    return shapexy_list

def new_char(n):
    return chr(ord('a') + (n%26))


def print_sol(v_h, s):
    val = parse(v_h, s)
    t_list = v_h.t_list
    K = v_h.K
    I = v_h.I
    J = v_h.J
    res = [[-1] * J for i in range(I)]
    for (k, l, i, j) in val:
        for (di, dj) in t_list.tetromino_list[l]:
            res[i+di][j+dj] = k
    for j in range(J-1, 0, -1):
        print('#', end='')
        for i in range(I):
            n = res[i][j]
            if n < 0:
                print(' ', end = '')
            else:
                print(new_char(n), end = '')
        print('#', end='\n')
    for i in range(I+1):
        print('#', end = '')
    print('#', end = '\n')
            
            
        
    
