# Describe the lists of tetrominos

import random

# 0: #
     #
     #
     #

# 1: ####

#################################

# 2: #
     #
     ##

# 3:   #
     ###

# 4: ##
      #
      #

# 5: ###
     #

#################################

# 6:  #
      #
     ##

# 7: ###
       #

# 8: ##
     #
     #

# 9: #
     ###

#################################
     
#10: #
     ##
     #

#11:  #
     ###

#12:  #
     ##
      #

#13:  #
     ###

#################################

#14: #
     ##
      #

#15:  ##
     ##

#################################

#16:  #
     ##
     #

#17: ##
      ##

#################################

#18: ##
     ##
      

class TetrominoList(object):
    tetromino_list = [
        [( 0, -1), ( 0,  0), ( 0,  1), ( 0,  2)], #0
        [(-1,  0), ( 0,  0), ( 1,  0), ( 2,  0)], #1
        [( 0, -1), ( 0,  0), ( 0,  1), ( 1, -1)], #2
        [(-1,  0), ( 0,  0), ( 1,  0), ( 1,  1)], #3
        [(-1,  1), ( 0, -1), ( 0,  0), ( 0,  1)], #4
        [(-1, -1), (-1,  0), ( 0,  0), ( 1,  0)], #5
        [(-1, -1), ( 0, -1), ( 0,  0), ( 0,  1)], #6
        [(-1,  0), ( 0,  0), ( 1, -1), ( 1,  0)], #7
        [( 0, -1), ( 0,  0), ( 0,  1), ( 1,  1)], #8
        [(-1,  0), (-1,  1), ( 0,  0), ( 1,  0)], #9
        [( 0, -1), ( 0,  0), ( 0,  1), ( 1,  0)], #10
        [(-1,  0), ( 0,  0), ( 0,  1), ( 1,  0)], #11
        [(-1,  0), ( 0, -1), ( 0,  0), ( 0,  1)], #12
        [(-1,  0), ( 0,  0), ( 0,  1), ( 1,  0)], #13
        [(-1,  0), (-1,  1), ( 0, -1), ( 0,  0)], #14
        [(-1, -1), ( 0, -1), ( 0,  0), ( 1,  0)], #15
        [(-1, -1), (-1,  0), ( 0,  0), ( 0,  1)], #16
        [(-1,  0), ( 0, -1), ( 0,  0), ( 1, -1)], #17
        [( 0,  0), ( 0,  1), ( 1,  0), ( 1,  1)]  #18
    ]
    
    L = len(tetromino_list)

    def get_rotation(self, l, d):
        ''' return the tetromino index corresponding to the rotation of
            the k-th tetromino in direction d = +1 or -1
        '''
        assert(d == 1 or d == -1)
        assert(0 <= l and l < self.L)
        if l < 2:
            return (l+d)%2
        elif l < 6:
            return ((l - 2 + d)%4) + 2
        elif l < 10:
            return ((l - 6 + d)%4) + 6
        elif l < 14:
            return ((l - 10 + d)%4) + 10
        elif l < 16:
            return ((l - 14 + d)%2) + 14
        elif l < 18:
            return ((l - 16 + d)%2) + 16
        else:
            return 18

    def random_tetromino(self):
        ''' Return a random tetromino with a random orientation.
        Uniform in the set of tetromino quotiented by their rotations '''
        choice = random.randrange(7)
        if choice == 0:
            orientation = random.randrange(2)
            return orientation
        elif choice == 1:
            orientation = random.randrange(4)
            return 2 + orientation
        elif choice == 2:
            orientation = random.randrange(4)
            return 6 + orientation
        elif choice == 3:
            orientation = random.randrange(4)
            return 10 + orientation
        elif choice == 4:
            orientation = random.randrange(2)
            return 14 + orientation
        elif choice == 5:
            orientation = random.randrange(2)
            return 16+orientation
        else:
            return 18
        
    def min_x_coordinate(self, l):
        ''' usefull for checking if tetromino l fit in the game board '''
        T = self.tetromino_list[l]
        return min([i for (i, _) in T])

    def max_x_coordinate(self, l):
        ''' usefull for checking if tetromino l fit in the game board '''
        T = self.tetromino_list[l]
        return max([i for (i, _) in T])

    def min_y_coordinate(self, l):
        ''' usefull for checking if tetromino l fit in the game board '''
        T = self.tetromino_list[l]
        return min([j for (_, j) in T])

    def max_y_coordinate(self, l):
        ''' usefull for checking if tetromino l fit in the game board '''
        T = self.tetromino_list[l]
        return max([j for (_, j) in T])

    def get_support_pos(self, l):
        ''' return of the positions that are just below tetromino l '''
        T = self.tetromino_list[l]
        T2 = [(i, j-1) for (i, j) in T]
        s = set(T)
        s2 = set(T2)
        return list(s2-s)
        
    
    def within_bounds(self, l, i, j, I, J):
        ''' tell if tetromino l lies within a board of dimensions I, J
        when placed at position i, j '''
        min_i = self.min_x_coordinate(l)
        max_i = self.max_x_coordinate(l)
        min_j = self.min_y_coordinate(l)
        max_j = self.max_y_coordinate(l)
        return(0 <= i + min_i and i+max_i < I and 0 <= j+min_j and j+max_j < J)
