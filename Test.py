from TetrominoList import*
from VarHandler import *
from VarHandler2 import *

def test_v_h(v_h):
    L = v_h.L
    K = v_h.K
    I = v_h.I
    J = v_h.J

    # Test Shape
    for l in range(L):
        for k in range(K):
            assert(v_h.inverse(v_h.shape(k, l)) == ("shape", (k, l)))
    # Test x
    for k in range(K):
        for i in range(I):
            assert(v_h.inverse(v_h.x(k, i)) == ("x", (k, i)))
    # Test y
    for k in range(K):
        for j in range(J):
            assert(v_h.inverse(v_h.y(k, j)) == ("y", (k, j)))
    # Test cell
    for k in range(K+1):
        for i in range(I):
            for j in range(J):
                assert(v_h.inverse(v_h.cell(k, i, j)) == ("cell", (k, i, j)))
    # Test shapexy
    for k in range(K):
        for l in range(L):
            for i in range(I):
                for j in range(J):
                    assert(v_h.inverse(v_h.shapexy(k, l, i, j)) == ("shapexy", (k, l, i, j)))
            
def test_v_h2(v_h):
    L = v_h.L
    K = v_h.K
    I = v_h.I
    J = v_h.J
    T = v_h.T
    test_v_h(v_h)

    # Test curr_shape
    for l in range(L):
        for t in range(T):
            assert(v_h.inverse(v_h.curr_shape(t, l)) == ("curr_shape", (t, l)))
    # Test curr_x
    for t in range(T):
        for i in range(I):
            assert(v_h.inverse(v_h.curr_x(t, i)) == ("curr_x", (t, i)))
    # Test curr_y
    for t in range(T):
        for j in range(J):
            assert(v_h.inverse(v_h.curr_y(t, j)) == ("curr_y", (t, j)))
    # Test shapexy
    for t in range(T):
        for l in range(L):
            for i in range(I):
                for j in range(J):
                    assert(v_h.inverse(v_h.curr_shapexy(t, l, i, j)) == ("curr_shapexy", (t, l, i, j)))
    # Test fixed
    for k in range(K):
        for t in range(T):
            assert(v_h.inverse(v_h.fixed(k, t)) == ("fixed", (k, t)))
    # Test fix
    for t in range(T):
        assert(v_h.inverse(v_h.fix(t)) == ("fix", (t)))
    
    
            
t_list = TetrominoList()
print("Testing VarHandler for K=5, I=5, J=8")
test_v_h(VarHandler(t_list, 5, 5, 8))
print("Done")
print("Testing VarHandler for K=20, I=8, J=12")
test_v_h(VarHandler(t_list, 20, 8, 12))
print("Done")

print("Testing VarHandler2 for K=5, I=5, J=8, T = 100")
test_v_h2(VarHandler2(t_list, 5, 5, 8, 100))
print("Done")
print("Testing VarHandler2 for K=20, I=8, J=12, T = 400")
test_v_h2(VarHandler2(t_list, 30, 4, 5, 31))
print("Done")
