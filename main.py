from FormulaBuilder import FormulaBuilder
from FormulaBuilder2 import FormulaBuilder2
from FileWriter import create_file_formula
from ParseOutput import print_sol
import subprocess
import random

#random.seed(0)
fb = FormulaBuilder(30, 8, 16)
n_var = fb.max_var_used()
formula = fb.whole_formula()
create_file_formula("formula.txt", n_var, formula)
#cp = subprocess.run(["z3", "-dimacs", "formula.txt"], capture_output = True, text=True)
#s = str(cp.stdout)
#print("10 tetrominos in 5 by 11 grid")
#print_sol(fb.v_h, s)

#fb2 = FormulaBuilder2(6, 5, 6, 10)
#n_var2 = fb2.max_var_used()
#formula2 = fb2.whole_formula()
#create_file_formula("formula2.txt", n_var2, formula2)
#cp2 = subprocess.run(["z3", "-dimacs", "formula2.txt"], capture_output = True, text=True)
#s2 = str(cp2.stdout)
#print("10 tetrominos in 5 by 11 grid, 100 steps")
#print_sol(fb2.v_h, s2)
