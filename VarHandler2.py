from VarHandler import VarHandler

class VarHandler2(VarHandler):
    
    def __init__(self, t_list, K, I, J, T):
        VarHandler.__init__(self, t_list, K, I, J)
        self.T = T

    def curr_shape(self, t, l):
        ''' curr_shape(t, l) returns a variable whose semantic is that the
        tetromino falling at time t is of type l '''
        assert(0 <= t and t < self.T)
        assert(0 <= l and l < self.L)
        return VarHandler.max_var(self) + 1 + t*self.L + l

    def curr_x(self, t, i):
        ''' curr_x(t, i) returns a variable whose semantic is that the
        tetromino falling at time t is centered on column i '''
        assert(0 <= t and t < self.T)
        assert(0 <= i and i < self.I)
        return self.curr_shape(self.T-1, self.L-1) + 1 + t*self.I + i

    def curr_y(self, t, j):
        ''' curr_y(t, j) returns a variable whose semantic is that the
        tetromino falling at time t is centered on line j '''
        assert(0 <= t and t < self.T)
        assert(0 <= j and j < self.J)
        return self.curr_x(self.T-1, self.I-1) + 1 + t*self.J + j

    def curr_shapexy(self, t, l, i, j):
        ''' curr_shapexy(t, l, i, j) is semantically equivalent to 
        curr_shape(t, l) and curr_x(t, i) and curr_j(t, j) '''
        assert(0 <= t and t < self.T)
        assert(0 <= l and l < self.L)
        assert(0 <= i and i < self.I)
        assert(0 <= i and j < self.J)
        return self.curr_y(self.T-1, self.J-1) + 1 + t * self.L*self.I*self.J + l*self.I*self.J + i*self.J + j
    
    def fixed(self, k, t):
        ''' fixed(k, t) means that the k-th tetromino is fixed at time t '''
        assert(0 <= t and t < self.T)
        assert(0 <= k and k < self.K)
        return self.curr_shapexy(self.T-1, self.L-1, self.I-1, self.J-1) + 1 + k * self.T + t

    def fix(self, t):
        ''' a fix event occured at date t '''
        assert(0 <= t and t < self.T)
        return self.fixed(self.K-1, self.T-1) + 1 + t
    
    def max_var(self):
        ''' return the maximal variable index used (= total number of variables) '''
        return self.fix(self.T-1)

    def inverse(self, v):
        ''' extends VarHandler.inverse to the new variables introduced in
        VarHandler2 '''
        assert(0 < v and v <= self.max_var())
        if v <= VarHandler.max_var(self):
            return VarHandler.inverse(self, v)
        elif v <= self.curr_shape(self.T-1, self.L-1):
            s = "curr_shape"
            vl = v - self.curr_shape(0, 0)
            t = vl//self.L
            l = vl - t*self.L
            return(s, (t, l))
        elif v <= self.curr_x(self.T-1, self.I-1):
            s = "curr_x"
            vl = v - self.curr_x(0, 0)
            t = vl//self.I
            i = vl - t*self.I
            return(s, (t, i))
        elif v <= self.curr_y(self.T-1, self.J-1):
            s = "curr_y"
            vl = v - self.curr_y(0, 0)
            t = vl//self.J
            j = vl - t*self.J
            return(s, (t, j))
        elif v <= self.curr_shapexy(self.T-1, self.L-1, self.I-1, self.J-1):
            s = "curr_shapexy"
            vl = v - self.curr_shapexy(0, 0, 0, 0)
            t = vl//(self.L*self.I*self.J)
            vl -= t * self.L*self.I*self.J
            l = vl//(self.I*self.J)
            vl -= l*self.I*self.J
            i = vl//self.J
            j = vl - i*self.J
            return(s, (t, l, i, j))
        elif v <= self.fixed(self.K-1, self.T-1):
            s = "fixed"
            vl = v - self.curr_shapexy(self.T-1, self.L-1, self.I-1, self.J-1) - 1
            k = vl//self.T
            t = vl - k*self.T
            return(s, (k, t))
        else:
            s = "fix"
            t = v - self.fix(0)
            return (s, (t))
            
            
