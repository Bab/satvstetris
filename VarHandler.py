class VarHandler(object):
    def __init__(self, t_list, K, I, J):
        '''
        t_list: tetromino list
        K: number of tetrominos
        L: number of different tetrominos (defined in TetrominoList)
        I: width of the game
        J: height of the game
        '''
        assert(0 < K and 0 < I and 0 < J)
        self.t_list = t_list
        self.K = K
        self.L = t_list.L
        self.I = I
        self.J = J
        

    # The next functions compute unique var numbers
    def shape(self, k, l):
        ''' shape(l, k) returns a variable whose semantics is that the
        k-th tetromino is of type l '''
        assert(0 <= l and l < self.L)
        assert(0 <= k and k < self.K)
        return 1 + k*self.L + l

    def x(self, k, i):
        ''' x(k, i) true iff the k-th tetromino is centered on column i'''
        assert(0 <= i and i < self.I)
        assert(0 <= k and k < self.K)
        return self.shape(self.K-1, self.L-1) + 1 + k*self.I + i

    def y(self, k, j):
        ''' x(k, j) true iff the k-th tetromino is centered on line j'''
        assert(0 <= j and j < self.J)
        assert(0 <= k and k < self.K)
        return self.x(self.K-1, self.I-1) + 1 + k * self.J + j
    
    def cell(self, k, i, j):
        ''' cell(k, i, j) true iff the cell of coordinate (i, j) is occupied
        at step k '''
        assert(0 <= k and k < self.K+1)
        assert(0 <= i and i < self.I)
        assert(0 <= j and j < self.J)
        return self.y(self.K-1, self.J-1) + 1 + k*self.I*self.J + i*self.J + j

    def shapexy(self, k, l, i, j):
        ''' shapexy(k, l, i, j) is semantically equivalent to shape(k, l) and x(k, i) and y(k, j). Used to shorten some formulas '''
        assert(0 <= k and k < self.K)
        assert(0 <= l and l < self.L)
        assert(0 <= i and i < self.I)
        assert(0 <= j and j < self.J)
        return self.cell(self.K, self.I-1, self.J-1) + 1 + k*self.L*self.I*self.J + l*self.I*self.J + i*self.J + j

    def max_var(self):
        ''' return the maximal variable index used (= total number of variables) '''
        return self.shapexy(self.K-1, self.L-1, self.I-1, self.J-1)
    
    def inverse(self, v):
        ''' tell how a variable v was constructed: return a 3-tuple with:
        - a string, either "shape", "x", "y", "cell" or "shapexy".
        - the arguments given to the function as a tuple
        '''
        assert(0 < v and v <= self.max_var())
        if v <= self.shape(self.K-1, self.L-1):
            s = "shape"
            vl = v-1
            k = vl//self.L
            l = vl - k*self.L
            return (s, (k, l))
        elif v <= self.x(self.K-1, self.I-1):
            s = "x"
            vl = v - self.shape(self.K-1, self.L-1) - 1
            k = vl//self.I
            i = vl - k*self.I
            return(s, (k, i))
        elif v <= self.y(self.K-1, self.J-1):
            s = "y"
            vl = v - self.x(self.K-1, self.I-1) - 1
            k = vl//self.J
            j = vl - k*self.J
            return(s, (k, j))
        elif v <= self.cell(self.K, self.I-1, self.J-1):
            s = "cell"
            vl = v - self.y(self.K-1, self.J-1) - 1
            k = vl//(self.I*self.J)
            vl = vl - k*self.I*self.J
            i = vl//self.J
            j = vl - i*self.J
            return(s, (k, i, j))
        else:
            s = "shapexy"
            vl = v - self.cell(self.K, self.I-1, self.J-1) - 1
            k = vl//(self.L*self.I*self.J)
            vl -= k * self.L*self.I*self.J
            l = vl//(self.I*self.J)
            vl -= l*self.I*self.J
            i = vl//self.J
            j = vl - i*self.J
            return(s, (k, l, i, j))
            
